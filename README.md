# Login-or-Signup

This is a Node.Js project that contains API for signup and login.

Follow the below steps for setup 

Install all npm modules/packages
$npm install npm-install-all

Set environment variable
	Mac: $export auth_jwtPrivateKey=mysecretkey
	Windows: $set auth_jwtPrivateKey=mysecretkey
	
Run Index file
$node index